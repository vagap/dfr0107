#include "DFR0107.h"

DRF0107::DRF0107(uint8_t _pin, unsigned int _remote_verify):
pin(_pin), remote_verify(_remote_verify)
{
	pinMode(pin, INPUT);
	digitalWrite(pin, LOW);
}

enum BUTTON DRF0107::get_ir_key()
{
	do {} //Wait for a start bit
	while(pulseIn(pin, HIGH) < BIT_START);

	return read_signal();
}

enum BUTTON DRF0107::get_ir_key(void (*func)(void))
{
	do {
		do {
			(*func)();
		} while (digitalRead(pin) == LOW);
	} while(pulseIn(pin, HIGH) < BIT_START);

	return read_signal();
}

enum BUTTON DRF0107::read_signal()
{
	int pulse[IR_BIT_LENGTH];
	int bits[IR_BIT_LENGTH];

	read_pulse(pulse);
	if (!this->pulse_to_bits(pulse, bits))
		return BUTTON_ERROR;
	return this->decode_bits(bits);
}

void DRF0107::read_pulse(int pulse[])
{
	for (int i = 0; i < IR_BIT_LENGTH; i++)
		pulse[i] = pulseIn(pin, HIGH);
}

boolean DRF0107::pulse_to_bits(int pulse[], int bits[])
{
	for(int i = 0; i < IR_BIT_LENGTH; i++) {
		if(pulse[i] > BIT_1) {
			bits[i] = 1;
		} else if(pulse[i] > BIT_0) {
			bits[i] = 0;
		} else
			return false;
	}
	delay(130);
	return true;
}

boolean DRF0107::RemoteVerify(int bits[])
{
	int result = 0;
	int seed = 1;

	for(int i = 0; i < FirstLastBit; i++) {		  
		if(bits[i] == 1)
			result += seed;

		seed <<= 1;
	}

	return (result == remote_verify);
}

enum BUTTON DRF0107::decode_bits(int bits[])
{
	int result = 0;
	int seed = 1;
	enum BUTTON button;

	for(int i = (IR_BIT_LENGTH-FirstLastBit); i < IR_BIT_LENGTH; i++) {		  
		if(bits[i] == 1)
			result += seed;

		seed <<= 1;
	}
	switch (result) {
		case BUTTON_POWER:
			button = BUTTON_POWER;
			break;
		case BUTTON_STOP:
			button = BUTTON_STOP;
			break;
		case BUTTON_LEFT:
			button = BUTTON_LEFT;
			break;
		case BUTTON_RIGHT:
			button = BUTTON_RIGHT;
			break;
		case BUTTON_PAUSE:
			button = BUTTON_PAUSE;
			break;
		case BUTTON_VOL_PLUS:
			button = BUTTON_VOL_PLUS;
			break;
		case BUTTON_VOL_MINUS:
			button = BUTTON_VOL_MINUS;
			break;
		case BUTTON_UP:
			button = BUTTON_UP;
			break;
		case BUTTON_DOWN:
			button = BUTTON_DOWN;
			break;
		case BUTTON_EQ:
			button = BUTTON_EQ;
			break;
		case BUTTON_REPT:
			button = BUTTON_REPT;
			break;
		case BUTTON_0:
			button = BUTTON_0;
			break;
		case BUTTON_1:
			button = BUTTON_1;
			break;
		case BUTTON_2:
			button = BUTTON_2;
			break;
		case BUTTON_3:
			button = BUTTON_3;
			break;
		case BUTTON_4:
			button = BUTTON_4;
			break;
		case BUTTON_5:
			button = BUTTON_5;
			break;
		case BUTTON_6:
			button = BUTTON_6;
			break;
		case BUTTON_7:
			button = BUTTON_7;
			break;
		case BUTTON_8:
			button = BUTTON_8;
			break;
		case BUTTON_9:
			button = BUTTON_9;
			break;
		default:
			button = BUTTON_ERROR;
			break;
	};
	return button;
}
