#include <DFR0107.h>

DRF0107 IR(2);

void setup() {
  Serial.begin(9600);
}

void loop() {
  BUTTON but = IR.get_ir_key();
  Serial.print("button: ");
  Serial.println(but);
  switch (but)
  {
    case BUTTON_POWER:  // turns on UUT power
      Serial.println("POWER");
      break;

    case BUTTON_STOP:  // FUNC/STOP turns off UUT power
      Serial.println("FUNC/STOP");
      break;

    case BUTTON_LEFT:  // |<< ReTest failed Test
      Serial.println("|<<");
      break;

    case BUTTON_PAUSE:  // >|| Test
      Serial.println(">||");
      break;

    case BUTTON_RIGHT:  // >>| perform selected test number
      Serial.println(">>|");
      break;

    case BUTTON_VOL_PLUS:  // VOL+ turns on individual test beeper
      Serial.println("VOL+");
      break;

    case BUTTON_VOL_MINUS:  // VOL- turns off individual test beeper
      Serial.println("VOL-");
      break;

    case BUTTON_DOWN:  // v scroll down tests
      Serial.println("v");
      break;

    case BUTTON_UP:  // ^ scroll up tests
      Serial.println("^");
      break;

    case BUTTON_EQ:  // EQ negative tests internal setup
      Serial.println("EQ");
      break;

    case BUTTON_REPT:  // ST/REPT Positive tests Select Test and Repeat Test
      Serial.println("ST/REPT");
      break;

    case BUTTON_0:  // 0
      Serial.println("0");
      break;

    case BUTTON_1:  // 1
      Serial.println("1");
      break;

    case BUTTON_2:  // 2
      Serial.println("2");
      break;

    case BUTTON_3:  // 3
      Serial.println("3");
      break;

    case BUTTON_4:  // 4
      Serial.println("4");
      break;

    case BUTTON_5:  // 5
      Serial.println("5");
      break;

    case BUTTON_6:  // 6
      Serial.println("6");
      break;

    case BUTTON_7:  // 7
      Serial.println("7");
      break;

    case BUTTON_8:  // 8
      Serial.println("8");
      break;

    case BUTTON_9:  // 9
      Serial.println("9");
      break;
     
    case BUTTON_ERROR:  // 9
      Serial.println("Error");
      break;
    break;
  }
}