#ifndef DRF0107_h
#define DRF0107_h

#include "Arduino.h"

#define IR_BIT_LENGTH 32    // number of bits sent by IR remote
#define FirstLastBit 15     // divide 32 bits into two 15 bit chunks for integer variables. Ignore center two bits. they are all the same.
#define BIT_1 1500          // Binary 1 threshold (Microseconds)
#define BIT_0 450           // Binary 0 threshold (Microseconds)
#define BIT_START 4000      // Start bit threshold (Microseconds)
#define DEFAULT_REMOTE_VERIFY 0x3F00

enum BUTTON {
	BUTTON_ERROR =		0x0000,
	BUTTON_POWER =		0x7F80,
	BUTTON_STOP =		0x7E81,
	BUTTON_LEFT =		0x7D82,
	BUTTON_RIGHT =		0x7C83,
	BUTTON_PAUSE =		0x7D02,
	BUTTON_VOL_PLUS =	0x7F00,
	BUTTON_VOL_MINUS =	0x7B04,
	BUTTON_UP =			0x7A85,
	BUTTON_DOWN =		0x7B84,
	BUTTON_EQ =			0x7906,
	BUTTON_REPT =		0x7887,

	BUTTON_0 =			0x7986,
	BUTTON_1 =			0x7788,
	BUTTON_2 =			0x7708,
	BUTTON_3 =			0x7689,
	BUTTON_4 =			0x758A,
	BUTTON_5 =			0x750A,
	BUTTON_6 =			0x748B,
	BUTTON_7 =			0x738C,
	BUTTON_8 =			0x730C,
	BUTTON_9 =			0x728D
};

class DRF0107 {
public:
	DRF0107(uint8_t _pin, unsigned int _remote_verify = DEFAULT_REMOTE_VERIFY);
	enum BUTTON get_ir_key();
	enum BUTTON get_ir_key(void (*func)(void));
private:
	uint8_t pin;
	unsigned int remote_verify;

	DRF0107() {};
	enum BUTTON read_signal();
	void read_pulse(int pulse[]);
	boolean pulse_to_bits(int pulse[], int bits[]);
	boolean RemoteVerify(int bits[]);
	enum BUTTON decode_bits(int bits[]);
};

#endif